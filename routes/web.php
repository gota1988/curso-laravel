<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(); 


Route::get('/home', 'HomeController@index')->name('home');

//checkage middleware
Route::group(['middleware' => ['auth']], function () {
    //->middleware(['checkage','auth']);
    Route::get('/contact','ContactsController@create')->name('viewcontact');
     Route::get('/list','ContactsController@index')->name('listcontacts');
    Route::post('/contact','ContactsController@store')->name('savecontact');
    Route::get('/editcontact/{id}','ContactsController@edit')->name('editcontacts')->where('id','^[0-9]+$');
    Route::put('/updatecontact/{id}','ContactsController@update')->name('updatecontact')->where('id','^[0-9]+$');
    Route::delete('/delete/{id}','ContactsController@destroy')->name('deletecontact')->where('id','^[0-9]+$');
    Route::get('/contacts/pdf','PdfController@crearpdf');
});

//Route::resource('contacts','ContactsController');




