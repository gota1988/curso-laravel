<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Auth;
use PDF;

class PdfController extends Controller
{
    public function crearpdf(){
        $contacts=new Contact();
        $user= \Auth::user();
        $pepe=$user->contacts()->get()->all();
        $pdf = PDF::loadView('pdf.contactspdf',compact('pepe'));
        return $pdf->download('contacts.pdf');

    }
}
