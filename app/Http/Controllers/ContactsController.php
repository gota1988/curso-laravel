<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Auth;
use Session;
use App\User;
use Storage;

use Illuminate\Support\Facades\Mail;
use \App\Mail\ContactMail;

class ContactsController extends Controller
{

    
    public function sendMail($request){
        $data=$request->all();
        Mail::to($data["email"],$data["nombre"])->send(new ContactMail($data));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objUser=new User();
        $user=\Auth::user()->id;
        $data=$objUser::find($user)->contacts()->get()->all();
        return view("contact.list")->with(['contacts'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //Session::forget('operacion');
       return view('contact.register');
    }


    public function saveImg($file){
      $name_file=time().'_'.$file->getClientOriginalName(); 
      if(Storage::disk('contactos')->put($name_file,file_get_contents($file->getRealPath()))){
        return $name_file;
      }else{
        return false;
      }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //public function store(\App\Http\Requests\contactRequest $request)
    public function store(Request $request)
    {
        $this->validate($request,[
            'nombre'=>'required',
            'email'=>'required|email',
            'telef'=>'required',
            'cumple'=>'required'
        ]);

        $user=Auth::user()->id;
        $objContact= new Contact();
        $valid=true;
        if(!empty($request->image)){         
             $name_file=$this->saveImg($request->image);
             if($name_file){
                $objContact->image=$name_file;
             }else{
                $valid=false;
             }
        }
        $objContact->user_id=$user;
        $objContact->name=$request->nombre;
        $objContact->email=$request->email;
        $objContact->phone=$request->telef;
        $objContact->birthday=$request->cumple;
        if($valid){
            if($objContact->save()){
                $this->sendMail($request);
                Session::flash('operacion','true');
            }else{
                //$request->session()->flash('operacion','false');
                Session::flash('operacion','false');
            }
        }else{
             Session::flash('operacion','false');
        }
        
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $objContact= new Contact();
        $data=$objContact::find($id);
        $user_id=\Auth::id();
        $user=User::find($user_id);
        // $this->authorize('update',$data);
        if(!is_null($data)){
            $data=$objContact->where('id',$id)->first();
            return view('contact.register')->with('data',$data);
        }else{
            return redirect()->route('listcontacts');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objContact= new Contact();
        $objContact=$objContact::find($id);
        $objContact->name=$request->nombre;
        $objContact->email=$request->email;
        $objContact->birthday=$request->cumple;
        $objContact->phone=$request->telef;
        if(!empty($request->image)){
            if(!is_null($objContact->image)){
                $this->deleteImg($objContact->image);
            }
            $name_file=$this->saveImg($request->image);
            if($name_file){
                $objContact->image=$name_file; 
            }  
        }
        if($objContact->save()){
            Session::flash('operacion','true');
        }else{
            Session::flash('operacion','false');
        }
        return redirect()->back();
    }

    public function deleteImg($nombre_imagen){
        if(\Storage::disk('contactos')->delete($nombre_imagen)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $objContact= new Contact();
       $objContact=$objContact::find($id);
       if(!is_null($objContact->image)){
        $this->deleteImg($objContact->image);
       }
       if($objContact->delete()){
            Session::flash('operacion','true');
        }else{
            Session::flash('operacion','false');
        }
        return redirect()->back();
    }
}
