<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Contact;

class ApiController extends Controller
{
    /**
    /**
     * [index description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        
        try {
           $objUser= new User(); 
           $objUser=$objUser::find($request->id);
           if(!is_null($objUser)){
            $data=$objUser->contacts()->get();
                return response()->json( [ 'msg'=>"success", 'data' => $data ],200);  
           }else{
                return response()->json( [ 'msg'=>"El usuario no existe", 'data'=>array() ],404);
           }
        } catch (Exception $ex) {
              \Log::error('Error al consultar contactos LINE: '.$ex->getLine().' FILE: '.$ex->getFile().'Message: '.$ex->getMessage());
             return response()->json( [ 'msg' => 'Error al crear contacto'],500);   
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $objContact=new Contact();
            $objContact->name=$request->name;
            $objContact->user_id=$request->user_id;
            $objContact->phone=$request->phone;
            $objContact->birthday=$request->birthday;
            $objContact->email=$request->email;
            if($objContact->save()){
                  return response()->json( [ 'msg'=>"success", 'data'=>array() ],201);
            }           
        } catch(\Illuminate\Database\QueryException $ex){
           
            \Log::error('Error al crear contacto LINE: '.$ex->getLine().' FILE: '.$ex->getFile().'Message: '.$ex->getMessage());
             return response()->json( [ 'msg' => 'Error al crear contacto'],500);  
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        try {
            $objContact=new Contact();
            $objContact=$objContact::find($id);
            $objContact->name=$request->name;
            $objContact->phone=$request->phone;
            $objContact->birthday=$request->birthday;
            $objContact->email=$request->email;
            if($objContact->save()){
                  return response()->json( [ 'msg'=>"success", 'data'=>array() ],200);
            }           
        } catch(\Illuminate\Database\QueryException $ex){
           
            \Log::error('Error al modificar contacto LINE: '.$ex->getLine().' FILE: '.$ex->getFile().'Message: '.$ex->getMessage());
             return response()->json( [ 'msg' => 'Error al crear contacto'],500);  
        }  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $objContact=new Contact();
            $objContact=$objContact::find($id);
            if(!is_null($objContact)){
                if($objContact->delete()){
                  return response()->json( [ 'msg'=>"success", 'data'=>array() ],200);
                }       
            }
            dd($objContact);
                
        } catch(\Illuminate\Database\QueryException $ex){
           
            \Log::error('Error al eliminar contacto LINE: '.$ex->getLine().' FILE: '.$ex->getFile().'Message: '.$ex->getMessage());
             return response()->json( [ 'msg' => 'Error al crear contacto'],500);  
        }  
    }
}
