<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $userage=Auth::user()->age;
        if($userage>=env('MAYORIA_EDAD')){
           return $next($request); 
        }else{
           \Abort(401);
        }
    }
}
