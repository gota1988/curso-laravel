<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'birthday', 'phone','user_id','email','image'
    ];


    function users(){
          return $this->belongsTo('App\User');
    }

}
