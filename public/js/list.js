$(document).ready(function(){
    let root=window.location.origin;
    let iduser=$("#app").attr("data-id");
    
    function consultar(){
         $.ajax({
          type: 'GET',
          //url: root+'/api/list/'+iduser,
          url: 'http://localhost/agenda/public/api/list/'+iduser,
          data: {},
          dataType: 'json',
          async: true,
          success:function(a){
            const resp=a.data;
            let tbody= $("#tb_contactos");
            tbody.empty();
            if(resp.length>0){
                 resp.forEach( function(element, index) {
                   let img=$("<img>",{src:'contactos/aboutMe_ico.svg',class:'imglist'})
                   if(element.image!=null){
                    img=$("<img>",{src:'contactos/'+element.image,class:'imglist'})
                   }
                   tbody.append(
                     $('<tr>').append(
                         $('<td>').text(element.id),
                         $('<td>').append(img),
                         $('<td>').text(element.name),
                         $('<td>').text(element.phone),
                         $('<td>').text(element.email),
                         $('<td>').append(
                            $("<a>",{class:'btn btn-primary btn-sm',href:'editcontact/'+element.id}).text('Modificar'),
                            $("<button>",{class:'ml-2 btn btn-danger btn-sm btn-delete','data-id':element.id}).text('Eliminar')
                         )
                      )
                    )
                });
             }else{
                 tbody.append(
                     $('<tr>').append(
                         $('<td>',{'colspan':'6',align:'center'}).text("sin datos"),
                      )
                    )
             }
        } 
      });
    }

    consultar();

    $('body').on('click', '.btn-delete', function() {
       let id=$(this).attr('data-id');
       let root=window.location.origin;
       $.ajax({
          type: 'POST',
          //url: root+'/api/delete/'+id,
          url: 'http://localhost/agenda/public/api/delete/'+id,
          data: {'_method':'DELETE'},
          dataType: 'json',
          async: true,
          success:function(a){
            if(a.msg=='success'){
                alert('Si!');
                consultar();

            }
          },
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
    });


})