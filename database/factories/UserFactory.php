<?php

use Faker\Generator as Faker;
use App\User;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'age' => $faker->randomDigit,
        'birthday' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$iY7id5heK1e5SAxtp0HFVO9YK5Yi7vgMtq4RnTR0D7pzSHyb91ndC',
        'remember_token' => str_random(10),
    ];
});
