@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Listar Contactos</div>
                <div class="card-body">
                   <a href="{{url('contacts/pdf')}}" class="btn btn-primary">Descargar PDF</a>
                   @if(session()->get("operacion")=="true")
                        <div class="alert alert-success" role="alert">
                          Operación realizada con exito
                        </div>
                    @endif
                    @if(session()->get("operacion")=="false")
                        <div class="alert alert-danger" role="alert">
                           Error! al guardar el contacto.
                        </div>
                    @endif
                    <table class="table">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Image</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Telefono</th>
                          <th scope="col">Email</th>
                          <th scope="col">Acciones</th>
                        </tr>
                      </thead>
                      <tbody id="tb_contactos">
                        {{-- @if(!empty($contacts))
                            @foreach($contacts as $detail)
                                <tr>
                                  <td>{{$detail->id}}</td>
                                    <td>
                                      @if(!is_null($detail->image))
                                        <img src="{{url('contactos/'.$detail->image)}}" alt="" class="imglist">
                                      @else
                                        <img src="{{url('contactos/aboutMe_ico.svg')}}" alt="" class="imglist">
                                      @endif
                                    </td>     
                                    <td>{{$detail->name}}</td>
                                    <td>{{$detail->phone}}</td>
                                    <td>{{$detail->email}}</td>
                                    <td>
                                        <a href="{{url('editcontact/'.$detail->id)}}" class="btn btn-primary btn-sm" >Modificar</a>
                                        <form action="{{route('deletecontact',$detail->id)}}" method="post" >
                                          <input type="hidden" name="_method" value="delete">
                                          @csrf
                                          <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                        <tr>
                            <td colspan="5">No se encontraron registros</td>
                        </tr>
                        @endif --}}
                      </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/list.js') }}"></script>
@endsection
@push('styles')
    <style>
    .imglist{
          width: 80px;
          height: 80px;
          border-radius: 50%;

      }
  </style>
@endpush