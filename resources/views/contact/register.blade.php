@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar Contacto</div>
                <div class="card-body">
                    @if(session()->get("operacion")=="true")
                        <div class="alert alert-success" role="alert">
                          Operación realizada con exito
                        </div>
                    @endif
                    @if(session()->get("operacion")=="false")
                        <div class="alert alert-danger" role="alert">
                           Error! al guardar el contacto.
                        </div>
                    @endif
                   
                   <form  method="post" action="{{!isset($data)?route('savecontact'):route('updatecontact',$data->id)}}" enctype="multipart/form-data" >
                      <div class="form-group">
                        <label for="nombre">Nombre</label>
                        
                          <input type="text" name="nombre" class="form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre"  placeholder="Nombre Completo" value="{{isset($data->name)?$data->name:null}}">
                             @if ($errors->has('nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                      {{--   @elsecannot
                        <span><b>{{isset($data->name)?$data->name:null}}</b></span>
                        @endcan --}}
                      </div>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email"  placeholder="ejemplo@ejemplo.com" value="{{isset($data->email)?$data->email:null}}">
                         @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="telef">Teléfono</label>
                        <input type="text" name="telef" class="form-control {{ $errors->has('telef') ? ' is-invalid' : '' }}" id="telef"  placeholder="2212-3233223" value="{{isset($data->phone)?$data->phone:null}}">
                         @if ($errors->has('telef'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('telef') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="cumple">Fecha de Cumpleanos</label>
                        <input type="date" name="cumple" class="form-control {{ $errors->has('cumple') ? ' is-invalid' : '' }}" id="cumple" value="{{isset($data->birthday)?$data->birthday:null}}"  >
                        @if ($errors->has('cumple'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cumple') }}</strong>
                            </span>
                        @endif
                      </div>
                       <div class="form-group">
                          <label for="image">Imagen</label>
                          <input type="file" name="image" class="form-control-file" id="image">
                        </div>
                        @if(isset($data->image) && !is_null($data->image))
                          <div class="form-group">
                            <img src="{{url('contactos/'.$data->image)}}" alt="" class="imgform">
                          </div>
                        @endif
                        @if(isset($data))
                            <input type="hidden" name="_method" value="PUT">
                        @endif
                      <a href="{{url('list')}}" class="btn btn-danger">Atras</a>
                      @if(isset($data))
                        @can('update',$data)
                          <button type="submit" class="btn btn-success">Modificar</button>
                        @endcan
                      @else
                           <button type="submit" class="btn btn-success">Guardar</button>
                      @endif
                      @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
